<?php

namespace Dsarhoya\EventManager;

use Aws\Sns\SnsClient;
use Dsarhoya\EventManager\Contract\EventContract;
use Dsarhoya\EventManager\Contract\EventPublisherContract;

class EventPublisher implements EventPublisherContract
{
    protected $snsClient;

    public function __construct(SnsClient $snsClient)
    {
        $this->snsClient = $snsClient;
    }

    public function publish(EventContract $event, string $topic): void
    {
        $message = [
            'name' => $event->getName(),
            'payload' => $event->getPayload(),
        ];

        $this->snsClient->publish([
            'Message' => json_encode($message),
            'TopicArn' => $topic,
        ]);
    }
}
