<?php

namespace Dsarhoya\EventManager\Contract;

interface EventHandlerContract
{
    /**
     * Parses incomming message body.
     *
     * @param string $message raw message body
     */
    public function handle(string $message): void;
}
