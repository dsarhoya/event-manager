<?php

namespace Dsarhoya\EventManager\Contract;

interface EventContract
{
    public function getName(): string;

    public function getPayload(): array;
}
