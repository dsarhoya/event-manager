<?php

namespace Dsarhoya\EventManager\Contract;

interface EventPublisherContract
{
    /**
     * Publish the given event to the specified topic.
     *
     * @param EventContract $event    event object to publish
     * @param string        $topicArn AWS SNS topic ARN
     */
    public function publish(EventContract $event, string $topicArn): void;
}
