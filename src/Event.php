<?php

namespace Dsarhoya\EventManager;

use Dsarhoya\EventManager\Contract\EventContract;

class Event implements EventContract
{
    protected $name;
    protected $payload;

    public function __construct(string $name, array $payload)
    {
        $this->name = $name;
        $this->payload = $payload;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPayload(): array
    {
        return $this->payload;
    }
}
