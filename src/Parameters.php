<?php

namespace Dsarhoya\EventManager;

class Parameters
{
    protected $topics;

    public function __construct(array $topics)
    {
        $this->topics = $topics;
    }

    public function getTopicArn(string $topic): ?string
    {
        return $this->topics[$topic] ?? null;
    }
}
