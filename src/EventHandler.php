<?php

namespace Dsarhoya\EventManager;

use Dsarhoya\EventManager\Contract\EventContract;
use Dsarhoya\EventManager\Contract\EventHandlerContract;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EventHandler implements EventHandlerContract
{
    protected $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function handle(string $message): void
    {
        $parsedMessage = $this->parseMessage($message);

        $event = $this->createEventFromParsedMessage($parsedMessage);

        if ($event instanceof EventContract) {
            $this->eventDispatcher
                ->dispatch(
                    $event,
                    $event->getName()
                )
            ;
        }
    }

    protected function parseMessage(string $message)
    {
        $message = json_decode($message, true)['Message']; // request body, json encoded

        return json_decode($message, true); // event, json encoded
    }

    protected function createEventFromParsedMessage($parsedMessage): ?EventContract
    {
        if (!is_array($parsedMessage)) {
            throw new \Exception('Argument passed must be of the type array, '.gettype($parsedMessage).' given.');
        }

        $name = $parsedMessage['name'] ?? null;
        $payload = $parsedMessage['payload'] ?? null;

        if (!isset($name) || !isset($payload)) { // if any of default event fields is missing, try to parse as an AWS event
            $name = $parsedMessage['source'] ?? null;
            $payload = $parsedMessage;
        }

        if (isset($name) && isset($payload)) {
            return $this->createEvent($name, $payload);
        }

        return null;
    }

    protected function createEvent(string $name, array $payload): EventContract
    {
        return new Event($name, $payload);
    }
}
