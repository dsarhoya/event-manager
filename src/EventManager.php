<?php

namespace Dsarhoya\EventManager;

use Dsarhoya\EventManager\Contract\EventContract;
use Dsarhoya\EventManager\Contract\EventHandlerContract;
use Dsarhoya\EventManager\Contract\EventPublisherContract;

class EventManager implements EventHandlerContract, EventPublisherContract
{
    protected $eventHandler;
    protected $eventPublisher;
    protected $parameters;

    public function __construct(
        EventHandlerContract $eventHandler,
        EventPublisherContract $eventPublisher,
        Parameters $parameters
    ) {
        $this->eventHandler = $eventHandler;
        $this->eventPublisher = $eventPublisher;
        $this->parameters = $parameters;
    }

    public function handle(string $message): void
    {
        $this->eventHandler->handle($message);
    }

    public function publish(EventContract $event, string $topic): void
    {
        $topicArn = $this->parameters->getTopicArn($topic);

        $this->eventPublisher->publish($event, $topicArn);
    }
}
