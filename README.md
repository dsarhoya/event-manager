# Event Manager

Un package para el manejo de mensajes (eventos) entre sistemas usando Amazon SNS.

Nota: la implementación por defecto de EventHandler no funciona correctamente con versiones 3.* de Symfony (cambia la interfaz EventDispatcherInterface).


# Uso
## EventManager::handle(string $message)

Recibe el mensaje, le hace parse y hace dispatch del evento de manera interna. Este evento se puede resolver luego con un EventSubscriber.

```
$eventManager->handle($rawRequestBody);
```

Cuando se recibe un mensaje desde *AWS EventBridge*, en el evento, "**name**" toma el nombre del campo "**source**" del mensaje.
Al "**payload**" del evento se le asigna el mensaje, decodificado como array, completo.

**Ejemplo:**
```
// JSON del evento de AWS

{
  "version": "0",
  "id": "6a7e8feb-b491-4cf7-a9f1-bf3703467718",
  "detail-type": "EC2 Instance State-change Notification",
  "source": "aws.ec2",
  "account": "111122223333",
  "time": "2017-12-22T18:43:48Z",
  "region": "us-west-1",
  "resources": [
    "arn:aws:ec2:us-west-1:123456789012:instance/i-1234567890abcdef0"
  ],
  "detail": {
    "instance-id": "i-1234567890abcdef0",
    "state": "terminated"
  }
}
```
El evento tendría "**name**" igual a "**aws.ec2**" y como payload el mensaje completo decodificado como arreglo.

## EventManager::publish(Event $event, string $topic)

Envía un mensaje con el evento al tópico SNS especificado (el tópico debe estar definido en *Dsarhoya\EventManager\Parameters*).

```
$event = new Dsarhoya\EventManager\Event($name, $payload);

$eventManager->publish($event, $topicName);
```

# Configuración de dependencias

## Bind de interfaz/implementación

Se deben de unir las interfaces *Dsarhoya\EventManager\Contract\EventHandler* y *Dsarhoya\EventManager\Contract\EventPublisher* con sus respectivas implementaciones.

## Definir dependencias
Se debe de configurar de manera manual la inyección de dependencias a las siguientes clases:

**Dsarhoya\EventManager\EventPublisher:** el cliente *Aws\Sns\SnsClient*.

**Dsarhoya\EventManager\Parameters:** un arreglo asociativo, donde la llave es el nombre identificador del tópico por el cual se accederá y el valor corresponde al ARN de este.

**Ejemplo:**

```
    Dsarhoya\EventManager\EventManager: ~
    
    Dsarhoya\EventManager\EventPublisher:
        arguments:
            $snsClient: '@sns_client'

    Dsarhoya\EventManager\Contract\EventPublisherContract: '@Dsarhoya\EventManager\EventPublisher'

    Dsarhoya\EventManager\EventHandler: ~
    Dsarhoya\EventManager\Contract\EventHandlerContract: '@Dsarhoya\EventManager\EventHandler'

    Dsarhoya\EventManager\Parameters: 
        arguments:
            $topics:
                default: '%env(DEFAULT_SNS_TOPIC_ARN)%'
```

## Extensión

Este package fue pensado para su extensión, utilizando contatos e implementando atributos de clase protegidos, 
a modo de facilitar el cambio de las implementaciones concretas.

De esta manera se puede, por ejemplo, remplazar la implementación concreta de *Dsarhoya\EventManager\Contract\EventPublisherContract* por una propia que 
luego de publicar el evento escriba en un log la acción realizada.

También se podría extender la clase *Dsarhoya\EventManager\EventHandler* y sobrescribir el método protegido *createEvent()* para que retorne una nueva clase que implemente *Dsarhoya\EventManager\Contract\EventContract*.
